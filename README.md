# Engage Summer School CYPRUS 2023

Goal is to perform unsupervised ML (dimensionality reduction + clsutering) for Acrylic polymer data.

![Polymer unit](polymer.png)


## Prerequisite

* Download anaconda, scikit learn, jupyter notebook, and numpy
(pip install -U scikit-learn and 
conda install numpy or pip install numpy)



* Clone the folder as: git clone https://gitlab.mpcdf.mpg.de/banerjeea/engage_school_2023.git




## Data Structure
* In the npz_file the PMMA sidechain-sidechain distances for each polymer chain (we have 100 chains, chain ID = 0,1, ... ,99) given across 11 different temperatures : '600','550','500','450', '400', '350', '300', '250', '200', '150', '100' (in Kelvin).

* The shape of each chainID_single_distance_ss.npz is (1111, 435) that is 11 temperatures $\times$ 101 frame for each chain; and 30 $\times$(30-1)/2 descriptors i.e. two side chains per monomer. 

# Main Task
1. Perform PCA stepwise (6 steps that will be discussed in the lecture).
2. Perform PCA with sklearn.
3. Compare the results.
4. Perform clustering on a single chain PCA spcace.
5. Average over all the chains. 



## ML glass transition temperature
Hands on exercise from 'Determining glass transition in all-atom acrylic polymeric melts
simulations using machine learning' https://XXXXXXX (Doi will be added, in revision J. Chem. Phys. 2023)


## If you want to access the actual gromacs trajectories check here:

Please download the homopolymer data from this link https://doi.org/10.17617/3.4JHOMW

## For the analysis of other polymers use the following github for further details:
https://gitlab.mpcdf.mpg.de/banerjeea/acrylic_polymers



## Feedback form

https://forms.gle/XpVzyaLD8PGfjAsG8
 
